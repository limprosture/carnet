Ce document liste des outils utiles à la prise de décision collectives non violentes. Merci à Camille.

Boîte à outils


Sommaire

Miroir
Cercle de parole
Réunions et célébrations
Rôles
Proposition du jour participative
Cercle de rêves / cercle d'intentions
Consentement / consensus
Gestion positive des tentions
Modes décisionnels
Communication Non-Violente
"J'ai dit"
Élection sans candidat·e
"[Prénom], je sais que tu as raison"

...................................................................................................................................................................................................................................................................................................................................................

Miroir
(ou Cadre)

C'est une base commune à laquelle on peut faire référence en cas de dérive.
Une sorte de filtre partagé appliqué dans toutes les interactions.

Exemple :
    
• Bienveillance (envers soi, le groupe, le lieu, le matériel)
→ La liberté des autres est prise en compte.
→ On ne manque pas de respect.

• Confidentialité (chacun·e doit être à l’aise d’exprimer ce dont elle·il a besoin, tout le monde accepte que certaines choses dites dans le cadre du groupe doivent y rester).

• Souveraineté ( chacun·e est responsable de soi, c’est à dire libre)
→ Pas de soumission
→ Responsabilisation

Avec ce cadre-là, tout le monde part du principe que chacun·e est bienveillant·e, souverain·e et respectueux·euse de la confidentialité.

.....................................................................................................................................................................................................................................................................................................................................................

Cercle de parole

    Pop-corn


    La parole est prise spontanément par qui à quelque chose de pertinent à dire.


    - Situation ou décision moins « importante »


    - Moins strict


    - Prise de parole inégale


    - Apport d’idées


    - À prioriser si le groupe se connaît bien et que cela ne nuira pas à la bonne participation de tout le monde


    Montre


    La parole est prise à tour de rôle dans un sens ou dans l’autre du cercle. On peut dire « je passe ».


    - Discussion ou décision d’un point important


    - Plus strict


    - Prise de parole « plus » égale (mais possibilité du « je passe »)


    - Importance de limiter le temps de parole (pour éviter que la discussion s’éternise)


    - Donne la possibilité aux personnes plus timides d’oser s’exprimer


    - Aide à instaurer de l’écoute, du respect et à mettre à l’écart l’ego (afin de favoriser l’efficacité des prises de décisions)


    - Possibilité d’utiliser un objet qui fait office de « droit de parole »


..........................................................................................................................................................................................................................................................................................................................................................

Réunion et célébrations

La réunion est un espace de prise de décisions, pas un moment de partage d’existence, il vaut mieux donc éviter d’y dire des choses qui ne sont pas importantes pour le groupe. Prendre du temps pour discuter de façon informelle est tout aussi primordial que de se réunir. 
À cet effet, peuvent être (pour ne pas dire doivent être) agendés des moments de célébrations pour s’octroyer du temps de fête hors-cadre de réunion afin de tisser plus de liens.

Les réunions peuvent être agendées à une date fixe (par exemple tout les 16 du mois) pour veillez à un « tournus » des absences.

..........................................................................................................................................................................................................................................................................................................................................................

Rôles

Les rôles sont créés en fonction des réponses données à la question :
De quoi a-t-on besoin ?
- pour aller dans le sens défini par le cercle de rêve
- pour mener à bien tel projet

Il existe plusieurs types de rôles. Lors des réunions, ils sont tournant afin que tous·tes puissent les expérimenter.
Quand il s’agit de travailler sur un projet, suivant la durée de celui-ci, ils peuvent être fixes.

Les rôles peuvent être distribués grâce à l’élection sans candidat·e·s .

Si besoin il y a, on peut être soutenu dans son rôle, cela demande de connaître ses limites et de les
accepter.
Pour chaque rôle, on défini un cahier des charges simple et concis, autrement dit une liste des
responsabilités. Quand la liberté est donnée aux gens de faire le travail à leur façon, celui-ci
devient une source de motivation et permet d’augmenter la confiance en soi. Associer liberté et
responsabilité implique de faire confiance et de lâcher prise sur nos « je-je-je ferais pas pareil ».
(décider du quoi mais pas du comment afin que la personne responsable soit libre d'agir à sa façon)

Voici quelques suggestions de rôles pour les réunions :
    
Facilitateur·trice est responsable de :
- Commencer et terminer la réunion à temps
- Organiser la proposition du jour (avec les points des autres membres)
- Présenter l’ordre du jour pour relecture
- Mettre des temps pour chaque sujet de discussion 
- Introduire chaque sujet et suggérer comment il doit être discuté
- Garder la discussion focalisée sur le sujet
- Coordonner le processus de prise de décision
- Amener chaque discussion à une fin
- Fermer la réunion en passant en revue que les décisions ont été prises et allouer un peu de temps pour l’évaluation de l’efficacité de la réunion
- Planifier la date et l’heure de la prochaine réunion
- Remercier les gens pour leur participation

Gardien·ne du temps et de l’espace est responsable de faire savoir au groupe lorsque le temps imparti à une discussion est bientôt écoulé (par ex. 5 min. avant)  et lorsqu’il est écoulé. En tant que gardien·ne de l’espace, iel veille à disposer la salle et les chaises un peu avant le début pour pouvoir commencer à temps.

Rapporteur·euse est responsable de prendre des notes détaillées des points et actions principales de la discussion, et d’en faire un compte rendu (PV) qui sera distribué aux participant·e·s et à celleux qui ont manqué la réunion.

Scribe est responsable d’écrire les idées sur un flip chart de sorte que chacun·e puisse être au clair avec ce qui a été dit. Dans les petits groupes cela peut ne pas être nécessaire, mais utile comme outil pour focaliser la discussion et clarifier les idées.

Gardien·ne des émotions est responsable d’observer les émotions dans la pièce. Iel essaie de s’assurer que chaque personne est vraiment écoutée.

Garant·e du bon déroulement du processus participatif vérifie que pour chaque proposition acceptée un tour de cercle a lieu pour que chacun·e s’exprime pour valider la décision. C'est un·e assistant·e à la personne qui facilite qui veille à ce que le processus soit respecté.

Maître·sse du snack est responsable des grignotages pendant la réunion, cela peut aider les gens à rester focalisés sur la discussion, particulièrement si la réunion a lieu le soir ou pendant les heures de repas.

................................................................................................................................................................................................................................................................................................................................................................

Proposition du jour participative

Document semi-permanent et participatif

Proposition et pas ordre car le but n'est pas d'être prisonnier·ère·s de l'outil

Idées :

    Utilisation de Framapad

    Ordre du jour dit participatif car il est préparé / modifié par tou·te·s·x en ligne

    Il y a un document de base (squelette de la réunion) qui est modifié à chaque fois

    Les point récurrents peuvent être mis en gras (météos, rappel des rôles, gestion des tensions, ...)

    Les modifications / ajouts doivent être fait avant la veille de la réunion (il est logique que plus les ajouts ont été tardifs, moins la personne qui facilite la séance pourra les avoirs inclus au mieux dans la proposition du jour

    Une personne, facilitateur·trice a le rôle de veiller au déroulement de la proposition du jour et à ce que tout le monde ait sa place

    Facilitateur·trice prépare la réunion (organisation, priorisation des éléments à aborder)


.................................................................................................................................................................................................................................................................................................................................................................

Cercle de rêves / cercle d'intentions

Le cercle de rêve permet de faire émaner la raison d’être évolutive du groupe. On peut aussi l’appeler charte / élan / intention / mission / vision / objectif ou but muable. Le mot « muable » place une nuance très importante qui permet d’éviter de percevoir l’objectif comme une violence en en devenant l’esclave. 
Il faut donc veillez à le renouveler ! Mais attention, pas en permanence. Il est utile de définir un moment, par exemple une fois par année, où l’on prend le temps de rediscuter la raison d’être.

Cercle de rêves

Tout le monde se pose intérieurement cette question :
Qu’est-ce qu’il faut pour que je me dise « c’est trop génial, je suis super content·e de faire partie de ce groupe ! » ?ç
La durée peut être définie afin de permettre à certaines personnes de mieux (ou pas) se projeter.

RÊVER ≠ DÉNI DE LA RÉALITÉ
N’ayons pas peur de rêver grand ensemble ! ...Et de rêver tout court ! On est déjà suffisamment confronté à la réalité.

Cercle d'intentions

Tout le monde réfléchit à des intentions qu'il·elle a pour le "projet" qui soient  formulées de façon à pouvoir compléter ces deux phrases :
    
J'ai l'intention de...
Et que chacun·e puisse ... 

(en commun pour les deux possibilités)

Il y a la possibilité de former des sous-groupes pour éviter de passer trop de temps à remettre en commun. 
1 groupe = 4 à 7 personnes

• À tour de rôle en cercle de parole, chacun·e donne une phrase synthétique qu’une personne se charge de retranscrire. Cette opération est à répéter 3 ou 4 fois. Une seule personne s’exprime à la fois.

• On relit les phrases. Si ce n’est pas clair ou qu’il y a un désaccord, on fait une discussion en cercle de parole pour arriver à une modification validée par l’autrice·teur de la phrase initiale. On adapte les phrases afin qu'elles vibrent pour tou·te·s·x (ou que chacun·e puisse vivre avec).

• Les phrases sont réorganisées dans un texte (ou pas) afin de donner forme à la charte muable du groupe. Il en émane des projets pour lesquels crée des rôles.

.................................................................................................................................................................................................................................................................................................................................................................

Consentement / consensus

Consentement : personne ne dit « non » ≠ Consensus : tout le monde dit « oui »

.................................................................................................................................................................................................................................................................................................................................................................

Gestion positive des tensions

Une tension c'est comme un petit caillou qu'on a dans la chaussure. Si on ne s'en occupe pas au plus vite il va nous faire de plus en plus mal et d'autres petits cailloux peuvent s'y ajouter à n'importe quand.

→ Créer un point récurrent à l'ordre du jour participatif afin de toujours avoir un espace réserver à l'expression de tensions.

.................................................................................................................................................................................................................................................................................................................................................................

Modes décisionnels

    majorité


    - Choix adopté si la majorité est d’accord

    - Rapide

    - Relativement représentatif

    - Non-participatif

    → risque de minorité mécontente


    Forme positive

    - Décisions peu importantes avec peu d’implications

    - Éviter de perdre de l’énergie


    Forme négative

    - Décisions fondamentales / stratégiques / importantes / implicatives


    minorité


    - Choix / décision faite par la majorité

    - Attention à la mise en péril du projet

    - Non-représentatif

    - Non-participatif

    → risque de majorité mécontente


    Forme positive

    - Expertise (les expert·e·s décident)

    - Éviter de perdre de l’énergie


    Forme négative

    - Oligarchie


    autocratie


    - Une seule personne décide

    - Rapide

    - Pas du tout représentatif


    Forme positive

    - Despotisme éclairé


    Forme négative

    - Dictature


    unanimité


    - Tout le monde est d’accord

    - Pas forcément rapide ou long

    - Engageant

    - Super représentatif

    → pas de frustration


    Forme positive

    - Consentement


    Forme négative

    - Consensus


    consultatif


    - Décision prise après consultation du groupe / des « sachant·e·s » / ...

    - Côté représentatif

    - Peu prendre du temps


    Forme positive

    - « sollicitation d’avis* »


    Forme négative

    - Opinion publique


    * Dans les entreprises libérées, la liberté n’est jamais séparée de responsabilité.

     « Tu peux prendre toutes les décisions qui doivent être prises quand tu as pris la peine de demander l’avis à toutes personnes compétentes et concernées. »


.................................................................................................................................................................................................................................................................................................................................................................

Communication NonViolente (CNV)
(Marshall Rosenberg)

    OSBD


    Observations → objectives

                                    cause du sentiment / interprétation / évaluation / jugement / critique / diagnostique / adjectifs / adverbes


    Sentiments → se réfère à des sensations corporelles

                               "faux-sentiments" qui pointent vers l'autre (trahi·e, incompris·e, attaqué·e, manipulé·e, humilié·e, ...)


    Besoins → universels (partagés par tou·te·s·x)

       intangibles (on ne peut les toucher, voir, prendre, ...)

       intrinsèques (n'impliquent jamais que quelqu'un·e de spécifique fasse quelque chose de spécifique)

       cause de tous nos sentiments

       " toute action est une tentative de combler un besoin "


    Demandes → concrètes (action, parole)

                              réalisables (pas un sentiment ou un changement d'habitude)

                              propositions (donc négociable, ce n'est pas une exigence)

                              conscience du " s'il te plaît "

                              en temps présent

                              langage positif

                              en lien avec les besoins

                              établir connexion empathique au préalable (faire comprendre que nos besoins comptent autant que ceux des autres)


.................................................................................................................................................................................................................................................................................................................................................................

"J'ai dit"
sert à signaler qu'on a fini notre intervention / explication / ...
→ utile pour éviter les coupages de parole

.................................................................................................................................................................................................................................................................................................................................................................

Élection sans candidat·e

C'est une façon de distribuer les rôles.
Plusieurs personnes peuvent remplir un rôle ensemble.
On peut demander du soutien dans un rôle.

    Tout le monde écrit sur un papier à qui iel attribuerait tel ou tel rôle

    Les résultats sont rassemblés (par scribe) et mis à la vue de tou·te·s·x

    À tour de rôle, chacun·e explique son vote

    Usage de l'outil cercle de paroles en montre jusqu'à un consentement sur l'élection


.................................................................................................................................................................................................................................................................................................................................................................

" [Prénom], je sais que tu as raison"

Pour signaler à autrui qu'iel est en train de jouer à "qui à raison" (voir Marshall Rosenberg)

_______________________________________________________________________________________________

* NOTE : le document ci-dessus est rédigé en écriture épicène

    iel(s) = il(s) et elle(s)

    celleux = celles et ceux

    le "x" dans par exemple "motivé·e·s·x" est pour inclure les personnes ne se reconnaissant pas plus comme femme que comme homme


_______________________________________________________________________________________________

SOURCES

    artisans du lien https://etiks.ch/


    UdN (université du Nous) http://universite-du-nous.org/


    Frédéric Laloux

    Reinventing Organizations, vers des communautés de travail inspirées http://www.reinventingorganizations.com/

    Conférence sur Reinventing Organizations https://www.youtube.com/watch?v=7jBagWc1Gwg


    Findhorn https://www.findhorn.org


    réflexions et ajout d'outils par le collectif cÂlinours (collectifcalinours@protonmail.ch)


