# Statuts de l'Improsture

## Définition

L'improsture est une troupe d'improvisation théâtrale présente sur le campus de l'Unil-EPFL. Elle se rattache à la sous-commission du PET, le PHIL.

## Objectifs

L'improsture vise à permettre à des improvisateurs et improvisatrices, novices ou experimenté·e·s, des occasions de jeu sous forme de spectacle. Elle vise également à permettre à chacun et à chacune de proposer des entraînements.
Pour atteindre ces objectifs elle :

- organise des entraînements hebdomadaire durant les semestres. Le lundi a été choisi comme date fixe.
- chercher à organiser au moins une fois par semestre académique un spectacle d'improvisation, si possible sous forme de concept original.
- recrute des membres, en cherchant notamment à veiller à la diversité :
    - de genre.
    - de style de jeu.
    - de niveau d'expérience en improvisation.
    - de milieu académique et/ou professionnel.

Elle veille à tenter de ne pas reproduire les schémas de domination de la société ou toutes autres discriminations.

## Appartenance

L'appartenance à l'Improsture n'est pas conditionnée à un rattachement académique au campus de l'UNIL-EPFL.

### Membres actif·ve·s

Le statut de membre actif·ve, à renouveller chaque année académique, s'acquiert :

- par le paiement de la cotisation annuel au PET.
- par la participation à au moins 3 entraînements par semestre, ou par la manifestation active du désir de participer à ces entraînements.
- l'adhésion explicite aux statuts de l'équipe, lors de la première adhésion à l'équipe.

Il se perd:

- par l'absence de cotisation ou par l'absence de participation à la vie d'équipe.


### Membres sympathisant·e·s

Toute personne le désirant peut être membre sympathisant·e et participer occasionnellement à des entraînements.

### Membres observateur·trice·s

Toute personne justifiant d'une expérience conséquente d'improvisation peut être membre observateur·trice·s, afin d'apporter ses conseils.

## Organisation et prise de décision

Les prises de décision sont prises lors d'une assemblée générale se déroulant au début de chaque semestre académique.
Lors de chaque assemblée générale, les membres de l'Improsture:

- votent les postes au sein de l'équipe de coordination.
- présentent un bilan des activités passées et des projets à venir.
- présentent un rapport de la trésorerie.
- discutent de possibles améliorations et de la vie au sein de l'équipe.

Sur demande d'au moins un quart des membres, une assemblée générale extraordinaire peut être convoquée.

Dans ses délibérations, l'assemblée générale préfère la recherche du consensus au vote, sans exclure toutefois le recours au vote.

Les délégations de pouvoir se font par mandats impératifs.

### Équipe de coordination

L'équipe de coordination est votée à chaque assemblée générale ordinaire, sauf en cas particulier où une partie de l'équipe de coordination peut être revotée lors d'une assemblée générale extraordinaire, ce qui nécessite un accord par un vote à majorité absolue.
L'équipe de coordination est organisée en responsables et pôles: 

- Un.e responsable des relations avec les autres équipes. 
- Un.e responsable communication interne qui se charge du bon fonctionnement de la communication au sein de l'équipe (discord et courriels).
- Un.e responsable communication externe qui s'occupe des réseaux sociaux et sites internet.
- Un.e responsable trésorerie.
- Un.e responsable ressources humaines qui se charge principalement de recruter de nouvelles recrues. 
- Un pôle entraînements composé de deux reponsables qui se chargent de trouver des entraineur.e.s
- Un pôle spectacles composé de deux reponsables qui se chargent d'organiser les spectacles et les déplacements. 
- (Temporaire) deux reponsables pour un camp d'entrainement d'été. 

L'équipe de coordination s'organise librement. Elle est tenue de rendre de communiquer de ses activités au reste de l'équipe. 

L'équipe de coordination est composée de volontaires. Toute personne peut quitter de son plein gré l'équipe de coordination.

### Groupe ad hoc

Pour l'accomplissement de tâches ponctuelles, l'AG peut déléguer, à sa propre initiative ou sous proposition de l'équipe de coordination, la réalisation d'une tâche pratique à un groupe ad hoc, qui se constitue pour la réalisation de cette tâche, et perd tout pouvoir une fois cette tâche accomplie.

### Gestion des conflits

En cas de conflits graves au sein de l'équipe, l'équipe de coordination cherche à résoudre ces conflits. Pour ce faire, elle essaie de produire un cadre de discussion propice à la communication non violente, ou chacun et chacune peut exprimer ses ressentis. Ce cadre vise à faire émerger entre les personnes en cause une modalité de sortie du conflit.

L'équipe de coordination peut s'appuyer sur des personnes externes pour tenter la résolution de conflit. En cas d'échec, elle peut proposer l'exclusion de l'un ou l'autre membre à l'Assemblée générale, qui statue alors dans un délai d'une semaine par un vote au scrutin majoritaire.

Si un·e ou plusieur·e·s membre de l'équipe sont impliqués dans le conflit, un groupe ad hoc, constitué de membres actif·ve·s extérieur·e·s au conflit, est formé.

### Situation d'oppression ou d'atteinte à la personnalité

En cas de situation d'oppression ou d'atteinte à la personnalité, psychique ou physique, par un·e ou plusieur·e·s membres de l'équipe sur un·e·s ou plusieurs·e·s membres de l'équipe, que ce soit dans ou hors du cadre de l'équipe, l'équipe de coordination ou n'importe quel membre de l'AG peut proposer à l'AG l'exclusion des personnes coupables. Celle-ci procède durant une semaine à un vote electronique au scrutin majoritaire.

Elle fournit également tout l'aide possible à la victime (ou aux victimes) pour se reconstruire.

### Conséquences de l'exclusion

L'exclusion implique :

- la perte d'accès aux canaux de communication électronique de l'équipe
- l'impossibilité de participer aux spectacles et entraînements

La réintégration dans l'équipe est possible:

- après accord de la ou les victimes, dans le cas d'exclusion pour oppression ou atteinte à la personnalité ;
 - après accord de l'AG dans les cas d'exclusion pour conflit.


