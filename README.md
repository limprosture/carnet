Ce carnet appartient à l'équipe d'Improvisation l'Improsture.

Nous y consignons nos grandes décisions, nos entrainements, nos spectacles.

# Comment écrire

Si tu es membre de l'équipe, tu peux demander un accès à l'équipe de coordination pour écrire dans ce carnet, notamment si tu donne un entrainement.

1. Navigue dans le carnet pour accéder au dossier où tu souhaite ajouter une page
2. Utiliser le petit + en haut du dossier pour créer un nouveau fichier
3. Le nom de ton fichier doit finir par `.md`
4. Tu peux aussi modifier un fichier existant en te rendant dessus puis en choisissant "Modifier"
5. Tu peux utiliser les raccourcis suivants (il s'agit du langage Markdown) :

`Ligne vide pour changer de paragraphe`

`#Titre de premier niveau (titre de la page)`

`##Titre de second niveau`

`### Titre de troisème niveau`

`**Gras**`
`*Italique*`

```
* Element de liste (précédé d'une ligne vide)
* Element de liste
* Element de liste
```

```
1. Element de liste (précédé d'une ligne vide)
2. Element de liste
3. Element de liste
```


Voir  [le guide pour bien commencer en Markdown](https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/)

Ou encore [la doc officiel du Markdown utilisé par Gitlab/Framagit](https://docs.gitlab.com/ee/user/markdown.html)
