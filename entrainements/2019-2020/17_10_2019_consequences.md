# Conséquences de nos actions

Entraîneur : Maïeul
But : poussée jusqu'au bout les conséquences de nos actions

## Théorie

Les actions (propos) peuvent avoir deux types de conséquences :

- des conséquences de l'ordre de la logique (le couple se dispute trop souvent, donc cela fait du bruit, donc les voisins se plaignent)
- des conséquences de l'ordre émotionnel (le couple se dispute trop souvent, donc X sombre en depression et Y est en colère)

Ces deux types de conséquences doivent être visibles en impro : l'histoire compte tout autant que les émotions qu'elle soulève (et réciproquement).

Par ailleurs les conséquences peuvent être petite ou faramineuse, (réalistes ou absurdes, etc).

On va essayer durant cet entrainement de jouer sur trois deux axes emotion/logique + réaliste / absurdes + petite / faramineuse.

Elles doivent être assumées.

## Échauffement + écoute

- Roi des grenouilles.
- 1,2,3,4,5,6,7 > avec contraintes. Si on perd, on tourne en criant "J'ai merdé, j'ai merdé, j'ai merdé"

## Entrainement par binome

- "Oui et" : une personne commence à raconter une histoire à l'autre, celle-ci va ajouter à l'element "Oui et" et amplifier l'element
- "Quand tu me dis / fais ca, cela me fait" : une personne dit quelque chose à l'autre où celle-ci est impliquée (en tant qu'acteur/trice) émotionellement. La personne répond "j'aime/je n'aime pas quand tu me dis cela car cela + ce que cela soulève comme émotion, souvenirs, etc, chez l'autre", et ca continue

## Entrainement en groupe

On commence à raconter une histoire, en ajoutant des conséquences logiques mais réaliste.

Puis on recommence avec la même histoire avec des conséquences logiques mais "invraisemblable".

Puis la même chose en désignant les émotions.

## Impro

Mini impros de 2 minutes. Chaque action / parole doit avoir des conséquences immédiates. Puis on refait l'impro, avec un element de changé.
