# 25 octobre 2018 : **les statuts**
Entraîneur : Maïeul  
Salle      : CE4  
Début      : 19h15  
Fin        : 21h30  

# Rappel théorique

- valeur numérique souvent entre 1 et 10 qui représente plus ou moins la *position hiérarchique* du personnage
- en impro le statut est différent du statut social du personnage
	- exemple : un maître peut avoir un statut inférieur à un valet (Fourberies de scapins) > ce qui fait souvent rire
- statut: outil de jeux qui peut donner du comique ou du tragique
- statuts pas nécessairement linéaires ex:
	- A est 10 vs B à 5
	- B est 10 vs C à 5
	- C est 10 vs A à 5
- statuts peuvent varier durant l'impro



# Échauffements

1. Echauffemnt corps à tour de role
2. Echauffement voix (secte)
3. Mot lancés
4. pass the clap

# Entrainement statut

On marche dans l'espace et on demande de faire plusieurs niveau de statuts.  
Changement attitude corporelle, démarche, voix.  
Deux groupes avec des status qui varient.

# Pause avant impro

Jeu d'écoute type balle de couleurs (pas réalisé cette séance).



# Impros

## Changement de status

Impro à 4 personnes type :
	- salle d'attente (dentiste)
	- repas (funérailles)
	- boulangerie

Chaque personne à un niveau de statut secret (tiré au sort).

Puis à x minutes les gens changent de statuts selon un autre statut tirés au sort.

## Statuts non linéaires

3 tableau de 2 personnes de type A/B/C. Cf plus haut. Les schémas n'est pas linéaire.

Ex:
	- employé·e / employeur·euse / amant·e
	- groupe de frères/sœurs
    - dans un sous-marin, militaires