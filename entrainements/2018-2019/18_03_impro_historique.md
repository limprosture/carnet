# Improvisation historique

Entraineur : Maïeul

## Théorie

- Dans "improvisation historique", il y a historique, mais il y a surtout "improvisation"
- Donc les bases de l'impro restent: écoute, relations, émotions, etc.
- L'aspect historique colore l'impro, mais il est évident qu'on ne peut pas être fidèle historiquement
- En fait, l'histoire sert de cadre pour dévelloper un imaginaire, imaginaire collectif connu par différent support (cours d'histoire, mais surtout roman, films, littérature etc)
- Si les gens veulement voir un récit historique, ils regardent Arte. Si on regarde de l'impro historique, on s'intéresse à un imaginaire.
- Donc pas de peur à avoir. On essaiera d'éviter les anachronismes (ou alors volontaires), mais on se complexera pas avec.

Un risque cependant avec l'impro historique est de se limiter aux "grandes figures", celle des puissants, des rois, des empereurs, etc. Hors l'histoire est faite de toutes les classes sociales. Le croisement de plusieurs niveau social dans de l'impro historique peut donner du poids.

Pour marquer l'époque:
- vocabulaire
- attitude
- intonation
- problématique rencontré

## Echauffement

Course d'obstacle
samourai ou autre jeu d'energie

Marcher dans la salle, chacun·e essaie de s'imprégner d'un personnage d'une période de son choix (Sauf contemporaine). Attitude, voix, passée, etc.
au bout d'un moment, interaction possible entre les gens qui marchent.


jeu d'écoute

puis goaler "de nazi" : le but est que l'époque  apparaisse clairement dans les 3 répliques

## Impro

impro de 2-3 minutes, avec une époque choisi en fonction des demandes du publics. Le milieu social de chaque personne peut être tiré au sort.

Puis en fin de séance "impro tag". On commence sur une période. Dès qu'on a une idée d'impulse sur une autre période, on prend la place (give and take).

## Liste de périodes

1. Invention de l'agriculture (préhistoire)
2. Égypte ancienne
3. Grèce classique (démocratie, philosophie, etc)
4. Rome antique (guerre des gaules, empire, etc.)
5. "Invasions barbares"
6. Croisades
7. Guerre de cent
8. Découverte de l'amérique
9. renaissance (art, imprimerie)
10. guerres de religion
11. Règnes de Louis XIII et Louis XIV (cours, complot, cape et épée, etc)
12. Siècle des lumières (nouvelles idées, philosophie, contestation)
13. Révolution francaise
14. Révolutions des barricades (trois glorieuses, 1848, etc)
15. révolution industrielle (misère, capitalisme, exploitation, Zola)
16. première Guerre mondiale (tranchée, pou, violence)
17. crise de 29-36 (misère, spéculation, front populaire)
18. guerre froide (course aux étoiles, CIA, complot, menace nucléaire)
19. contemporain
20. futur (tout est possible)

je censure volontairement la seconde guerre mondiale en raison de complexe perso sur cette période.

## Typologie de niveau social

1. "Big boss" : empereur/impératrice, PDG, président·e etc
2. Premier cercle : conseiller·ere, ministre
3. Haute classe sociale : noblesse, grande bourgeoisie,  cadre sup, courtisans, etc.
4. Moyenne classe sociale : petite bourgeoisie, petite noblesse de province affaiblie, etc
5. Basse classe : prolétariat, serfs, esclave, etc.
6. Sous classe : mendiants, lumpenproletariat, etc.
