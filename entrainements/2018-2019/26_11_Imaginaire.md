# Entraînement du 26 novembre 2018: **Imaginaire**

Entraîneur : Iann
## But de l'entraînement

Créer des impros à partir de lieux imaginés / Arriver sur scène en _faisant_ une action spécifique plutôt que de _seulement_ parler.

## Échauffements
- Allongés par terre en étoile (les têtes se touchent), les membres du groupe décrivent les éléments visuels d'un tableau imaginaire, puis de la pièce dans laquelle il se trouve, puis des interactions entre les deux
- Passage de courant: quelqu'un est "chargé" et passe le courant à quelqu'un d'autre en lui touchant la main. Le courant passe à travers le corps de la personne touchée. Il faut rapidement toucher son autre main pour recevoir le courant

## Impros
1. **Scene-painting simple.**
Chacun·e décrit (aussi précisément que possible) un objet présent dans une pièce

2. **Scene-painting avec personnage.**
En plus des objets, on ajoute à la description autant de personnes que de participant·e·s. Une fois le décor posé, chacun·e peut choisir de prendre la place d'un personnage et d'exprimer ses pensées

3. **Scene-painting avec petites impros**
Même que précédent, mais les joueurs/euses peuvent soit exprimer les pensées, soit interagir avec d'autres personnages présents dans la pièce

4. **Scene painting construit durant une impro.**
Chaque joueur/euse peut choisir soit de poser un objet, soit de jouer un personnage. L'impro se développe par l'ajout d'objets ou de personnages et par l'interaction entre personnages