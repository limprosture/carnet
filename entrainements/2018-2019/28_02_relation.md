# Entraînement du 4 février 2019: **Relations**

Entraîneur : Maïeul

## But de l'entraînement

- Poser rapidement des relations
- Poser des relations "intéréessantes"
- Poser des relations dans un contexte historique précis

## Échauffements

- Corps : courses, on rencontres obstacles et notamment des gens
- Ecoute : compter jusqu'à xx ensemble
- Idée : "j'ai besoin de"

## Rappel théorique

- Une relation à une nature et une "couleur". Ex de nature : couple. Exemple de couleur : ielles se supportent pas, ielles sont trop attachés, etc.
- La relation doit être posée et explicitée très tôt, mais sans gros sabot
- la relation n'est pas figée, et c'est bien souvent son évolution qui va donner de l'interet à l'impro

## Impros

- Goaler sévère : en 3 répliques poser la nature et la couleur des relations

- Impro 5/10 min avec archetype de relations posée avant (par tirage au sort):
	- la nature de la relation est connu des 2 improvisateurices
	- la couleur de la relation est spécifique à chaque perso, donc secrete
	- si au bout d'une minute la relation n'est pas clair pour le public, levé la main, Si au bout de 2 minutes tjr pas clair, on va pas plus loin et on debrief
- Impulse : entrée de dictionnaire historique

## Archetype de relation


### Nature

1. Couple
2. Parent / enfant (tout âge)
3. Frère / sœur
4. Ami·e·s
5. Ennemie·s
6. Collègue
7. Maître / disciple, prof / élève etc
8. Patron·ne / employé, maître / esclave

## Couleur

1. Amour (toutes ses nuances)
2. Haine
3. Peur / crainte
4. Jalousie
5. Admiration
6. Degôut

