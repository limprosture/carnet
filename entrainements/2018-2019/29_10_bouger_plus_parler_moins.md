# 29 octobre 2018 : **Bouger plus, parler moins**
Entraîneur : Guillaume 
Salle      : CE4  
Début      : 19h15  
Fin        : 21h30  

# Rappel théorique

- Le regard est très important en impro. Tout ne doit pas forcément passer par la parole. 
- Cet entrainement va nous montrer qu'on peut faire passer énormément d'émotion sans parler. 
- Il faut par contre se concentrer énormément sur ce que font les autres lorsqu'il n'y a pas de parole. Etre attentif! 

# Échauffements

1. Chacun propose un échauffement.
2. Echauffement "à la Antoine"
3. (TOUT AU LONG DE LA SEANCE) "3 respirations". On fait une première respiration pour soi, puis une pour ses voisins, et une dernière pour tout le monde.

# Entrainement mouvementé

1. Jeu du sniper. Une personne est désignée secrètement comme "sniper". Elle peut assassiner ses ennemis avec des clins d'oeil. 
S'il reste 3 personnes, le sniper gagne. A tout moment, une personne peut désigner qui est le sniper, mais si elle se trompe son équipe perd.

# Impros

1. "Ce que tu fais, c'est mou!" Une personne rentre dans un lieu et fait une impro en silence. 
La personne suivante doit faire la même impro mais en exagérant ce qu'elle a vu. Et ainsi de suite.
2. "Impro par récurrence". Une personne rentre dans une pièce et fait une action. La personne doit répéter cette action et en proposer une nouvelle. Et ainsi de suite. 
3. "Musique-Lieu-Personnage". Une musique est mise en fond. Chacun peut rentrer et participer à l'impro en s'inspirant de la musique. Le lieu, les personnages et les objets doivent être assez clairs. Important d'appuyer les personnages ou les objets proposés en les répétant (deux ou trois personnes répètent la même action). 
