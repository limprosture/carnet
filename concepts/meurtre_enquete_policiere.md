# Meutre et enquête policière

Proposition par Maïeul

## Intérêt

- tenir le suspens
- autour d'un meutre et d'une enquete se noue tout une série de relation:
	- proches de la victime avec la victime
	- proches entre eux
	- enquêteurs/trices entre elleux
	- enquêteurs/trices avec la victime

# En pratique

Il y a des scènes types dans une enquête policière sur la longue durée

- Découverte de la victime
- Interrogatoire
- Discussions entre les enquêteurs
- Discussions entre les proches

On peut donc partir de là pour l'entrainement.

Pour les impulses, j'imagine ceci :

- on demande un genre, un âge et un métier pour la victime
- un lieu de découverte pour la victime
- une arme du crime
- deux enqueteurs/trices
- quand une personne (sauf enquêteur/trices) rentre sur scène pour la première fois, on demande la relation de la personne avec la victime
