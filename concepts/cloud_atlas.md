# Cloud Altas

Concept basé sur le film éponyme.

*Proposé par*: Laurent

## Format

Long form avec entracte. Avant la pause, `k` histoires (`k >= 2`) sont racontées à moitié. Chaque histoire se déroule dans une période de l'histoire (ou du futur) dans un style particulier et à un lieu précis, sans flashback, éventuellement avec de courtes ellipses et (si possible) sans _side-stories_. Les histoires sont racontées dans l'ordre chronologique. Après la pause, les histoires sont terminées l'une après l'autre, mais dans l'ordre inverse. A la fin du spectacle, toutes les histoires devront être connectées.

## Exemple

Imaginons 4 histoires (`k=4`) dans 4 périodes différentes (sur impulsion du public). Imaginons que nous avons:

* Histoire 1 : La révolution française
* Histoire 2 : La deuxième guerre mondiale en Russie
* Histoire 3 : La Grande Bretagne en 2019
* Histoire 4 : Le Japon en 2042

Cela nous donnera donc 4 histoires et 8 parties d'histoire (4 avant la pause et 4 après) pour le déroulement suivant (par exemple):

1. Histoire 1, partie 1 : Drame en France durant la révolution.
2. Histoire 2, partie 1 : Histoire d'amour à Leningrad.
3. Histoire 3, partie 1 : Thriller dans le contexte du Brexit.
4. Histoire 4, partie 1 : Comédie musicale de robots a Okinawa.
5. Entracte
6. Histoire 4, partie 2
7. Histoire 3, partie 2
8. Histoire 2, partie 2
9. Histoire 1, partie 1

Il devrait y avoir un lien entre chaque histoire, de préférence de différents types. Par exemple, un tableau peint par l'un des personnages dans l'histoire 1 est à la base de la rencontre des amoureux dans l'histoire 2. Le petit fils du couple de l'histoire 2 est le héros du thriller de l'histoire 3. Une invention dans l'histoire 3 est à l'origine de la profession d'un personnage de l'histoire 4. Si possible, ces liens ne seront pas tous révélés avant la pause.

## Version avec thématique donnée

On peut imaginer donner une thématique précise (d'actualité) au spectacle. Par exemple, les femmes de tête, la lutte des classes, le véganisme, etc.

## Voir aussi

[https://fr.wikipedia.org/wiki/Cartographie_des_nuages](Page Wikipedia du libre Cloud Atlas)

