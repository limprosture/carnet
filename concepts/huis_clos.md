# Huis clos

Le principe d'un huis clos est d'enfermer des personnages dans un lieux plutot étroit (mais pas trop, il faut plusieurs pièces) pour voir comment les gens réagissent entre eux.

On pourrait commencer ainsi :

- demander au public un lieu contenant plusieurs pièces, mais assez petit (ex. maison, gare, train, mais pas Université)
- demander au public les pièces
- demander pour quel raison les gens pourraient être enfermeé (ex : mauvais temps, catastrophe nucléaire, panne du train)

Lorsque deux personnes entre sur scème pour la première fois on peut demander:

- soit de préciser la pièce
- soit de préciser leur fonction/relation
