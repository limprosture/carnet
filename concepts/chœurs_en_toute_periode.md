# Chœur en toute période

Ce concept permet de meler historique et adresse public.
Concept testé il y a deux ans à des entrainements estivaux de l'Heidi
C'est plutot du middle forme : 4 * 20 minutes.
Il joue plus sur les aspects théâtraux.

Chaque histoire, indépendante, a lieu à une période différente.

En début d'histoire, une personne "chœur" fait une adresse public qui pose la situation initiale. Par exemple
"Rome, 64 après Jésus-Christ. Toute la ville à brûlé. La population s'est refugié dans les jardins de l'Empereur. Un couple d'amoureux se recherche désepairement dans cette foule désesperé."
Cela pose l'enjeux et le contexte.

Puis scènes normales (avec éditions, et tout et tout).

Si le chœur voit que cela "patine" il peut intervenir pour relancer l'intrigue en apportant un nouvelle élèmen "L'empereur lui-même décide de rendre visite au peuple".

En fin d'impro, le chœur revient et apporte une conclusion "Et c'est ainsi que, par une simple histoire d'amour, Néron mourut prématurement"


