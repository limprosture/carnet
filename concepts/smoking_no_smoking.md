## SMoking / No Smoking

## Idée originelle
Impro longue, plutot. Inspiré du film du même nom

petite scène d'exposition. Puis à un moment, une action est prise qui va faire basculer le scénario.
Une autre action est prise ensuite, qui pourra refaire basculer le scénario.
Puis on refait "et si cette action n'avait pas eu lieu"

````
                 +---------------------->    Mettre le feu au musée
                 |
                 +
+---------->   Fumer         +---------->    Réussir à éteindre l'incendie et passer pour un héros
|
|
|
|
+---------->   Ne pas fumer  +---------->  Séduire
                    +
                    |
                    |
                    +------------------->  Ne pas séduire
````


Pour aider à la compréhension, nous pourrions avoir un graphe que l'on trace au fur et à mesure.

## FOrme simplifié
Une variante plus simple est tout simplement de faire une histoire longue avec des interventions du public, sur demande du MC, pour choisir entre deux options à des moments clés.

Exemple : si on voit qu'une personne s'apprête à faire une déclaration d'amour, on demande au public s'il veut qu'il la fasse ou pas.
