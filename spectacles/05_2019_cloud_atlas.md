# Spectacle cloud Atlas

Lieu : Zélig

Date : 20 mai 2019 20h

Participant·e·s :
- Maïeul
- Anne-Sophie
- Chloé
- Ariane
- Guillaume
- Yann
- Laurent
- Quentin

- MC : Basile?
- Musique : Zélig
- Light : Zélig
- Relation à Zélig : Maïeul
- Collation : Guillaume + Chloé

Durée : 2 * 40 minutes + 15 minutes de pause

Prix : chapeau (tenu par Roxane)
## Concept

Décris dans le [document "Cloud Atlas"](../concepts/cloud_atlas.md).

## Réalisation pratique du concept

#### 1. Comment détermine-t-on les époques ?

Varier le contraste entre les époques. Max 3 choix par époques.
- A. Antiquité :
	-	Egypte
	-	Grèce
	-	Empire Romain,
	- Invasion Barbare
- B. Epoque contemporaine :
	- Révolution Francaise,
	- 1er guerre mondiale
	- Entre deux guerres
	- 2eme guerre Guerres Mondiales
	- Guerre Froide
- C. Aujourd'hui (env. 1990-2020) : demander un lieu au public, en choisir un
- D. Futur :
	-	dystopie
	-	post apocalyptique
	-	conquete spatiale

### 2. Comment gère-t-on la transition entre les époques

Chaque mi temps de 40 min  envi. Première mi temps 4 impros de 8 à 12 minutes. Deuxième mi temps, on peut passer plus rapidement d'une période à l'autre, en variant le rythme (type "impro tag").

Avantage de deuxième partie : variations de :
- rythme
- relation entre époque
- effet d'époque

En première mi temps ordre chronologique A, B, C, D. En deuxième mi temps on demande où l'on reprend.

En première mi temps,  annonce publique de l'époque avec une phrase type "Nouvelle époque". Si MJ, c'est idéal que ce soit lui (à tester).

Des noirs entre les époques en première mi temps. Il faut que lorsqu'on change d'époque, on annonce clairement au light qu'on veut le noir.

Bien marquer par le jeu et l'ambiance les différentes époques.
#### 3. As-t-on des éditions de scène au sein d'une époque

Oui c'est obligé, mais évitons les flash back au sein d'une époque.

#### 4. Comment détermine-t-on les impulses, en dehors des époques.

Pas d'impulse spécifique

#### 5. Quels style de jeu / d'histoire veut-on ? sérieux? comique? dramatique? tragique?


On laisse libre, mais il faut que ca soit cohérent à l'intérieur d'une scène. Cf problème de constructions. Il est bon que cela soit contrasté entre les époques.

### 6. Quels sont les liens entre les époques

- Cela peut être :
	- objets
	- lieux
	- personnages auxquels on fait allusion d'une époque à l'autre, mais pas de revenant ou de machine à voyager dans le temps
- Il faut que ce qui se passe dans une impro influence l'époque suivante.
- Par contre il ne faut pas le même lien pour tout les époque
- Il faut pas voir le lien directement lorsqu'on passe d'une époque à l'autre, pas dans les 30 secondes
- Les liens peuvent être dans la première partie.
- <!> Pas de voyage dans le temps

### 7. Maitre·ss·e de cérémonie ? ou pas

Oui
## Publicité

Affiches :
	- on peut imprimer via LAurent
	- Chloé + Basile : logo + affiche
Reseau sociaux:
	- Event Zelig
	- rediger de présentation du concept pour Zelig
# CR réunion 2 avril rencontre avec Zélig + Maïeul

représentante de Zélig : Emelyne

- Horaire de début : 19h30 ou 20h. Mais a priori plutot 20h, car on ne peut pas aménager la salle avant 19h
- Loge pour échauffement:
	- Bureau : on peut y poser nos affaires et la bouffe mais c'est un peu petit pour s'echauffer
	- salles de cours où on stocke les canapés -> plus grande, mais pas dispo avant 19h
	- vu la saison on pourrait s'échauffer dehors
- Tarifs:
	- 2 personnes de Zélig qui nous aide : 100 CHF
	- Techniques : entre 0 et 300 CHF si on fait appel ou pas à leur matos et leur équipe, Maïeul écrit un mail pour négocier
	- Affiches : à imprimer nous mêmes
	- On peut faire payer jusqu'à 5 CHF ou mettre un chapeau
- Bar : le bar est fermé durant le spectacle, il est ouvert avant et après, et à l'entracte
- Aménagement : si on est 8 personnes, on met 1/4 pour déménager les canapés.
- On peut remplir entre 80 et 100 personnes
- Peut être un contrat à signer, mais pas sûr, et a priori plutot non
- Light : ils ont le système. Si on amène quelqu'un, cela nous coute moins chers
- Son : idem, ils sont leur système. Si on amène quelqu'un, voire le système du PET
- Publicité :
	- Zélig crée un évènement facebook > il faut qu'on rédige un texte de présentation
	- ils collent  une semaine avant dans tout le campus unil + à sat. A nous de coller à EPFL, a priori
	- il faut qu'on prépare affiche en mettant notre logo, celui de zelig, l'horaire, le titre, le prix

=> Maïeul contact pour avoir plus de précison sur les questiopns de sous.
