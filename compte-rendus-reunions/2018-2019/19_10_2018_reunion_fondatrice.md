#Présent·e·s
Fabio, Mario, Thibaut R, Camille B., Iann, Maïeul, Laurent, Camillo (par téléphone)

#Idée:
  - 19h15
  - entrainement lundi ou jeudi ou en alternance
  - framadate a remplir d'ici dimanche pour choisir la date https://framadate.org/s7L9hQfEFd4o3Rq6
  - 1er entrainement la semaine prochaine (22 au suivant)
  - il faut que ce soit régulier sinon c'est un +Impro bis
  - autonentrainement
    - si jeudi de la semaine prochaine > Maïeul
    - si lundi (Ariane?)
  - la personne qui donnerait l'entraînement annonce un peu l'avance la thématique pour que les gens puissent réagir si cela ne convient pas
  - 1 fois par semestre un·e intervenant·e externe
    - pour le premier semestre le pote de Camille > elle voit avec lui
  - Groupe Telegram pour communiquer > Laurent
  - Premier semestre : créer l'équipe et faire connaissance
  - Second semestre : concevoir ensemble un spectacle + définir un nom d'équipe
  - EPFL CE1, CE2, CE3

#Questions :
  - financement d'un intervenant externe (1 fois / semestre ?)

#Piste pratique:
  - un endroit pour centraliser des idées d'entraînement (carnet numérique commun d'entrainement).
  - noter les thématiques des séances / suivi des personnes présentes.

#Groupes de travail:
  - Informatique : Yann, Camille, Maïeul
  - Finance : Fabio